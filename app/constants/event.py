STORY_CREATED = 'story_created'
USER_CREATED = 'user_created'
EDIT_STORY = 'story_edited'
DELETE_STORY = 'story_deleted'
EDIT_COMMENT = 'comment_edited'
DELETE_COMMENT = 'comment_deleted'
EDIT_FEATURED_STORY = 'feature_story_edited'
EDIT_THEN_AND_NOW = 'then_and_now_edited'
STORY_FLAGGED = 'story_flagged'
